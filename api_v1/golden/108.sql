PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE soundscape (id INTEGER PRIMARY KEY,name TEXT,tempo REAL,meter_nominator INTEGER,meter_denominator INTEGER);
INSERT INTO "soundscape" VALUES(108,'Relax',2.0,4,4);
CREATE TABLE location (id INTEGER PRIMARY KEY,soundscape_id INTEGER,latitude REAL,longitude REAL,radius REAL,volume REAL,distance_scale_parameter REAL);
INSERT INTO "location" VALUES(986,108,4.1418910101514867959e+01,2.17132598161697387695e+00,23.0,0.836566,0.0165959);
INSERT INTO "location" VALUES(987,108,4.14189201583598887168e+01,2.17137694358825683593e+00,73.0,0.333043,0.0331131);
INSERT INTO "location" VALUES(988,108,4.14189141242530496373e+01,2.1713930368423461914e+00,55.0,0.414715,0.0234423);
INSERT INTO "location" VALUES(992,108,4.14187351121642492995e+01,2.17186510562896728515e+00,12.0,1.0,0.1);
INSERT INTO "location" VALUES(993,108,4.14185983385501188536e+01,2.17148959636688232421e+00,12.0,1.0,0.1);
INSERT INTO "location" VALUES(994,108,4.14186486230006067899e+01,2.17110067605972290039e+00,12.0,1.0,0.1);
INSERT INTO "location" VALUES(995,108,4.14189101015150953335e+01,2.17140644788742065429e+00,41.0,0.584454,0.1);
PRAGMA writable_schema=ON;
INSERT INTO sqlite_master(type,name,tbl_name,rootpage,sql)VALUES('table','location_index','location_index',0,'CREATE VIRTUAL TABLE location_index USING rtree (id, min_latitude, max_latitude, min_longitude, max_longitude)');
CREATE TABLE "location_index_node"(nodeno INTEGER PRIMARY KEY, data BLOB);
INSERT INTO "location_index_node" VALUES(1,X'0000000700000000000003DA4225ACBF4225AD2D400AF27C400AFB8700000000000003DB4225AC4D4225ADA6400AE97F400B062F00000000000003DC4225AC764225AD7B400AED4B400B02EA00000000000003E04225ACAB4225ACE6400AFD7A400B023300000000000003E14225AC874225ACC3400AF753400AFC0C00000000000003E24225AC954225ACCF400AF0F4400AF5AC00000000000003E34225AC964225AD59400AF044400B00620000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
CREATE TABLE "location_index_rowid"(rowid INTEGER PRIMARY KEY, nodeno INTEGER);
INSERT INTO "location_index_rowid" VALUES(986,1);
INSERT INTO "location_index_rowid" VALUES(987,1);
INSERT INTO "location_index_rowid" VALUES(988,1);
INSERT INTO "location_index_rowid" VALUES(992,1);
INSERT INTO "location_index_rowid" VALUES(993,1);
INSERT INTO "location_index_rowid" VALUES(994,1);
INSERT INTO "location_index_rowid" VALUES(995,1);
CREATE TABLE "location_index_parent"(nodeno INTEGER PRIMARY KEY, parentnode INTEGER);
CREATE TABLE sound (id INTEGER PRIMARY KEY,data TEXT);
INSERT INTO "sound" VALUES(773,'http://api.dev.locosonic.com/system/uploads/location_sound/file/773/gitarre.wav');
INSERT INTO "sound" VALUES(775,'http://api.dev.locosonic.com/system/uploads/location_sound/file/775/filtered.wav');
INSERT INTO "sound" VALUES(776,'http://api.dev.locosonic.com/system/uploads/location_sound/file/776/filtered2.wav');
INSERT INTO "sound" VALUES(781,'http://api.dev.locosonic.com/system/uploads/location_sound/file/781/newdrums.wav');
INSERT INTO "sound" VALUES(782,'http://api.dev.locosonic.com/system/uploads/location_sound/file/782/bass.wav');
INSERT INTO "sound" VALUES(783,'http://api.dev.locosonic.com/system/uploads/location_sound/file/783/ewspr_03.wav');
INSERT INTO "sound" VALUES(784,'http://api.dev.locosonic.com/system/uploads/location_sound/file/784/ewspr_02.wav');
INSERT INTO "sound" VALUES(785,'http://api.dev.locosonic.com/system/uploads/location_sound/file/785/ewspr_12.wav');
CREATE TABLE sound_file_info (sound_id INTEGER,channels INTEGER,sample_rate REAL,frames INTEGER);
CREATE TABLE location_sound (id INTEGER PRIMARY KEY,location_id INTEGER,sound_id INTEGER,volume REAL,onset_quantization REAL,onset_quantization_phase REAL,offset_quantization REAL,offset_quantization_phase REAL,duration REAL,repetitions REAL);
INSERT INTO "location_sound" VALUES(846,986,776,0.61624,-1.0,0.0,-2.0,0.0,0.0,0.0);
INSERT INTO "location_sound" VALUES(857,987,782,0.857531,-1.0,0.0,-1.0,0.0,-3.0,0.0);
INSERT INTO "location_sound" VALUES(858,988,781,0.739605,-1.0,0.0,-1.0,0.0,0.0,0.0);
INSERT INTO "location_sound" VALUES(859,992,783,0.251189,-1.0,0.0,0.0,0.0,0.0,1.0);
INSERT INTO "location_sound" VALUES(861,993,784,0.125748,-1.0,0.0,0.0,0.0,0.0,1.0);
INSERT INTO "location_sound" VALUES(862,994,785,0.232675,-1.0,0.0,0.0,0.0,0.0,1.0);
INSERT INTO "location_sound" VALUES(863,995,773,0.398107,-1.0,0.0,0.0,0.0,0.0,0.0);
INSERT INTO "location_sound" VALUES(872,986,775,0.679204,-1.0,0.0,-4.0,0.0,0.0,0.0);
PRAGMA writable_schema=OFF;
COMMIT;
