PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE soundscape (id INTEGER PRIMARY KEY,name TEXT,tempo REAL,meter_nominator INTEGER,meter_denominator INTEGER);
INSERT INTO "soundscape" VALUES(176,'SoundGuide-Vienna-DE Schwendenplatz',1.0,4,4);
CREATE TABLE location (id INTEGER PRIMARY KEY,soundscape_id INTEGER,latitude REAL,longitude REAL,radius REAL,volume REAL,distance_scale_parameter REAL);
INSERT INTO "location" VALUES(1985,176,4.82119202136118900608e+01,1.63787816490433897338e+01,25.0,1.0,0.1);
INSERT INTO "location" VALUES(1986,176,4.82118965021649898989e+01,1.63779608406036736312e+01,25.0,1.0,0.1);
INSERT INTO "location" VALUES(1987,176,4.82120583147565682233e+01,16.3773654110726,25.0,1.0,0.1);
INSERT INTO "location" VALUES(1988,176,4.82119167287668517465e+01,16.3773761216472,107.0,1.0,0.0104713);
INSERT INTO "location" VALUES(1989,176,4.82116488543187742248e+01,1.63786420740033236143e+01,25.0,1.0,0.1);
INSERT INTO "location" VALUES(1990,176,4.82124526232107299952e+01,1.63765339172462880417e+01,25.0,1.0,0.1);
INSERT INTO "location" VALUES(1991,176,4.82122808062354692984e+01,1.63769631084171223282e+01,25.0,1.0,0.1);
PRAGMA writable_schema=ON;
INSERT INTO sqlite_master(type,name,tbl_name,rootpage,sql)VALUES('table','location_index','location_index',0,'CREATE VIRTUAL TABLE location_index USING rtree (id, min_latitude, max_latitude, min_longitude, max_longitude)');
CREATE TABLE "location_index_node"(nodeno INTEGER PRIMARY KEY, data BLOB);
INSERT INTO "location_index_node" VALUES(1,X'0000000700000000000007C14240D8C54240D93D4183070D4183087000000000000007C24240D8C04240D9384183055F418306C200000000000007C34240D8E94240D961418304274183058A00000000000007C44240D8044240D9FD418301E8418307D300000000000007C54240D87E4240D8F7418306C44183082700000000000007C64240D9524240D9CA41830273418303D600000000000007C74240D9254240D99D41830354418304B70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
CREATE TABLE "location_index_rowid"(rowid INTEGER PRIMARY KEY, nodeno INTEGER);
INSERT INTO "location_index_rowid" VALUES(1985,1);
INSERT INTO "location_index_rowid" VALUES(1986,1);
INSERT INTO "location_index_rowid" VALUES(1987,1);
INSERT INTO "location_index_rowid" VALUES(1988,1);
INSERT INTO "location_index_rowid" VALUES(1989,1);
INSERT INTO "location_index_rowid" VALUES(1990,1);
INSERT INTO "location_index_rowid" VALUES(1991,1);
CREATE TABLE "location_index_parent"(nodeno INTEGER PRIMARY KEY, parentnode INTEGER);
CREATE TABLE sound (id INTEGER PRIMARY KEY,data TEXT);
INSERT INTO "sound" VALUES(1938,'http://api.dev.locosonic.com/system/uploads/location_sound/file/1938/Right.wav');
INSERT INTO "sound" VALUES(1939,'http://api.dev.locosonic.com/system/uploads/location_sound/file/1939/leftright1648.aif');
INSERT INTO "sound" VALUES(1940,'http://api.dev.locosonic.com/system/uploads/location_sound/file/1940/scary.mp3');
INSERT INTO "sound" VALUES(1941,'http://api.dev.locosonic.com/system/uploads/location_sound/file/1941/wrong-right.wav');
INSERT INTO "sound" VALUES(1942,'http://api.dev.locosonic.com/system/uploads/location_sound/file/1942/Race_Robot_-_left');
INSERT INTO "sound" VALUES(1943,'http://api.dev.locosonic.com/system/uploads/location_sound/file/1943/Untitled-13.wav');
INSERT INTO "sound" VALUES(1944,'http://api.dev.locosonic.com/system/uploads/location_sound/file/1944/synthetic_pings__light-bulb__indicator.flac');
INSERT INTO "sound" VALUES(1945,'http://api.dev.locosonic.com/system/uploads/location_sound/file/1945/user_left.wav');
INSERT INTO "sound" VALUES(1946,'http://api.dev.locosonic.com/system/uploads/location_sound/file/1946/proceed_direct.wav');
CREATE TABLE sound_file_info (sound_id INTEGER,channels INTEGER,sample_rate REAL,frames INTEGER);
CREATE TABLE location_sound (id INTEGER PRIMARY KEY,location_id INTEGER,sound_id INTEGER,volume REAL,onset_quantization REAL,onset_quantization_phase REAL,offset_quantization REAL,offset_quantization_phase REAL,duration REAL,repetitions REAL);
INSERT INTO "location_sound" VALUES(2270,1985,1938,1.0,0.0,0.0,0.0,0.0,-4.0,0.0);
INSERT INTO "location_sound" VALUES(2271,1986,1939,1.0,0.0,0.0,0.0,0.0,0.0,0.0);
INSERT INTO "location_sound" VALUES(2272,1986,1940,1.0,0.0,0.0,0.0,0.0,0.0,1.0);
INSERT INTO "location_sound" VALUES(2273,1986,1941,1.0,0.0,0.0,0.0,0.0,0.0,1.0);
INSERT INTO "location_sound" VALUES(2274,1987,1942,1.0,0.0,0.0,0.0,0.0,-4.0,0.0);
INSERT INTO "location_sound" VALUES(2275,1988,1943,1.0,0.0,0.0,0.0,0.0,0.0,0.0);
INSERT INTO "location_sound" VALUES(2276,1989,1944,1.0,0.0,0.0,0.0,0.0,-1.0,0.0);
INSERT INTO "location_sound" VALUES(2277,1990,1945,1.0,0.0,0.0,0.0,0.0,-4.0,0.0);
INSERT INTO "location_sound" VALUES(2278,1991,1946,1.0,0.0,0.0,0.0,0.0,-4.0,0.0);
PRAGMA writable_schema=OFF;
COMMIT;
